# userscripts

## How to use:
Install [violentmonkey](https://violentmonkey.github.io/) then click on a raw user.js to get an installation dialog.

## Nyaa
### JAVLibraryButton
Adds a button to sukebei.nyaa pages to search the code in JAVLibrary. Works about 80% of the time.

[Install](https://codeberg.org/of60693/userscripts/raw/branch/master/nyaa/javlibrary_button/main.user.js)

### R18 Nyaa Checker
Adds a button on R18 pages to search for the code on sukebei.nyaa. (Most of the time there is no results T_T)

[Install](https://codeberg.org/of60693/userscripts/raw/branch/master/nyaa/r18_nyaa_checker/main.user.js)