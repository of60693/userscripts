// ==UserScript==
// @name         JAVLibrary Button
// @namespace    https://codeberg.org/of60693/userscripts
// @version      1.0
// @description  Adds a button to sukebei.nyaa.si pages to search on javlibrary
// @author       of60693
// @match        https://sukebei.nyaa.si/view/*
// @icon         https://www.google.com/s2/favicons?domain=sukebei.nyaa.si
// @grant        none
// ==/UserScript==

const baseURL = 'https://www.javlibrary.com/en/vl_searchbyid.php?keyword='

const sleep = ms => new Promise(res => setTimeout(res, ms));

const getJAVLibraryURL = () => {
    // find the DVD ID in the page
    const title = document.querySelector('div.panel-heading').firstElementChild.innerText;
    // regex to find the ID in the form of "letters-numbers"
    const match = title.match(/\w+\-\d+/);
    if (match) {
        // split on the -
        const id = match[0].split('-');
        // return the ID with padded zeros
        return baseURL + id[0] + id[1].padStart(5, '0');
    }
    return null;
}

const addJAVLibraryButton = () => {
    const button = document.createElement('button');
    button.textContent = 'JAVLibrary Search';
    const url = getJAVLibraryURL();
    if (!url) {
        return false;
    }
    button.onclick = () => window.open(url);
    document.querySelector('div.panel-heading').appendChild(button);
    return true;
}

const continuousFind = async () => {
    console.log('Starting search...');
    let tries = 0;
    while (!addJAVLibraryButton()) {
        tries++;
        if (tries > 5) {
            console.log("Too many retries. Aborting.");
        }
        console.log("Didn't find element. Trying again in 3 seconds...");
        await sleep(3000);
    }
    console.log('Found DVD ID.')
}

continuousFind();

