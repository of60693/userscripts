// ==UserScript==
// @name         R18 Nyaa Checker
// @namespace    https://codeberg.org/of60693/userscripts
// @version      1.0
// @description  Adds a button to R18 pages to search on sukebei.nyaa.si
// @author       of60693
// @match        https://www.r18.com/videos/*
// @icon         https://www.google.com/s2/favicons?domain=r18.com
// @grant        none
// ==/UserScript==

const sleep = ms => new Promise(res => setTimeout(res, ms));

const addSearchButton = () => {
    // find DVD-ID header
    const tags = document.getElementsByTagName("h3");
    const searchText = "DVD ID";
    let dvdID = null;

    for (let tag of tags) {
        if (tag.textContent === searchText) {
            dvdID = tag;
            break;
        }
    }

    if (!dvdID) {
        return false;
    }
    // get value of dvd id
    let code = dvdID.parentNode.childNodes[1];
    // add link to search Nyaa
    let aTag = document.createElement('a');
    aTag.setAttribute('href', "https://sukebei.nyaa.si/?q=" + code.textContent);
    aTag.innerText = "\nSearch on Sukebei";
    code.appendChild(aTag);

    return true;
}

const continuousFind = async () => {
    console.log('Starting search...');
    let tries = 0;
    while (!addSearchButton()) {
        tries++;
        if (tries > 5) {
            console.log("Too many retries. Aborting.");
        }
        console.log("Didn't find element. Trying again in 3 seconds...");
        await sleep(3000);
    }
    console.log('Found DVD ID.')
}

continuousFind();



